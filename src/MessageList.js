import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

  
class MessageList extends React.Component{
    render(){
    
        return(
            <div>
            <CssBaseline />
            <Container fixed>
                 <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '100vh' }} />
            </Container>
            
            </div>    
           
        )
    }
}

export default (MessageList);
