import React from 'react'
import TextField from '@material-ui/core/TextField';
import Send from '@material-ui/icons/Send';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';

const styles = theme => ({
    button: {
        margin: theme.spacing(1),
    },
    textField:{
        marginLeft: '13%', 
        padding: '10px',
       
    }
   
});

class SendMessageForm extends React.Component{
    render() {
        const { classes } = this.props;

        return(
            <FormGroup row >
                <TextField
                   id="outlined-full-width"
                   style={{width: '70%'}}
                   className= {classes.textField}
                   placeholder="Type a message..."
                   margin="normal" 
                   variant="outlined"    
               />
               <Button color="primary" >
                    <Send/>
               </Button>    
            </FormGroup> 
        ); 
    }
}

export default (withStyles(styles))(SendMessageForm);
