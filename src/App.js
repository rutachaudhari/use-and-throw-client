import React from 'react';
import MessageList from './MessageList'
import './App.css';
import SendMessageForm from './SendMessageForm'
import ButtonAppBar from './ButtonAppBar'

function App() {
  return (
    <div className="App" >
    <ButtonAppBar />
    <MessageList />
    <SendMessageForm />
  </div>
  );
}

export default App;
